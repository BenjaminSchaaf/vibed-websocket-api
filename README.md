# vibe.d WebSocket API

## Example

```d
interface WebSocketServerAPI {
    int getUserId(string name);
}

interface WebSocketClientAPI {
    string challengeResponse(string challenge);
}

class WebSocketServer : WebSocketAPI {
    int getUserId(string name) {
        auto client = new WebSocketInterfaceClient!(WebSocketClientAPI, JsonRPC)(rpcWebSocket);

        if (client.challengeResponse("foo") == "bar") {
            return 3;
        } else {
            return -1;
        }
    }
}

auto router = new URLRouter;
router.registerWebSocketInterface!JsonRPC(new WebSocketServer);


class WebSocketClient : WebSocketClientAPI {
    string challengeResponse(string challenge) {
        assert(challenge == "foo");

        return "bar";
    }
}

auto api = new WebSocketInterfaceClient!(WebSocketServerAPI, JsonRPC)("ws://example.com", new WebSocketClient);

int id = client.getUserId("foo"); //=> 3

string js = generateWebSocketClient!(JsonRPC, WebSocketAPI);
```
