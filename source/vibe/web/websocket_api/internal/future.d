module vibe.web.websocket_api.internal.future;

import std.traits;

import vibe.d;

@safe:

class Future(R) {
    Task task;
    R result;
    Exception exception;

    @property bool ready() { return !task.running; }

    R getResult() {
        task.join();

        if (exception) throw exception;
        return result;
    }
}

Future!(ReturnType!CALLABLE) async(CALLABLE, ARGS...)(CALLABLE fn, ARGS args) {
    auto future = new Future!(ReturnType!CALLABLE);

    future.task = runTask(() {
        try {
            future.result = fn(args);
        } catch (Exception e) {
            future.exception = e;
        }
    });

    return future;
}
