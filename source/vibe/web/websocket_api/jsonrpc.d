module vibe.web.websocket_api.jsonrpc;

import core.time;

import std.meta;
import std.traits;

import vibe.d;

import vibe.web.websocket_api;
import vibe.web.websocket_api.internal.future : async, Future;

@safe:

class ParserException : Exception {
    static const code = -32700;

    this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) {
        super(msg, file, line, nextInChain);
    }
}

class InvalidRequestException : Exception {
    static const code = -32600;

    Json callID = Json.undefined;

    this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) {
        super(msg, file, line, nextInChain);
    }
}

class MethodNotFoundException : InvalidRequestException {
    static const code = -32601;

    this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) {
        super(msg, file, line, nextInChain);
    }
}

class InvalidParamsException : InvalidRequestException {
    static const code = -32602;

    this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) {
        super(msg, file, line, nextInChain);
    }
}

class CustomJsonRPCException : InvalidRequestException {
    long code;

    this(long code, string msg, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) {
        super(msg, file, line, nextInChain);

        this.code = code;
    }
}

void enforceJsonRPC(bool expression, long code, lazy string msg = "Enforcement Failed") {
    if (!expression) throw new CustomJsonRPCException(code, msg);
}

final class JsonRPC(Interface) : Protocol {
    private {
        size_t idCounter = 0;

        enum isVoid = is(Interface == void);
    }

    static if (!isVoid) {
        Interface instance;

        this(Interface instance) {
            this();

            this.instance = instance;
        }
    }

    this() {}

    private size_t nextID() {
        const result = idCounter;
        idCounter += 1;

        return result;
    }

    override void handleMessage(RPCWebSocket rpcWebSocket, scope IncomingWebSocketMessage message) {
        const contents = readAllUTF8(message);

        Json json;
        try {
            json = parseJsonString(contents);
        } catch (Exception e) {
            throw new ParserException("Message is not Json");
        }

        // Check jsonrpc version number
        auto versionPtr = "jsonrpc" in json;
        enforce!InvalidRequestException(versionPtr !is null, "Missing Json RPC version number");

        auto _version = (*versionPtr).to!string;
        enforce!InvalidRequestException(_version == "2.0", "Only Json RPC version 2.0 is supported");

        // Get optional ID
        auto idPtr = "id" in json;
        Json id;
        if (idPtr !is null) {
            id = *idPtr;
        }

        try {
            auto methodPtr = "method" in json;
            if (methodPtr is null) {
                handleResponse(rpcWebSocket, json);
            } else {
                static if (!isVoid) {
                    auto method = (*methodPtr).to!string;

                    handleCall(rpcWebSocket.webSocket, json, method);
                }
            }
        } catch (InvalidRequestException exception) {
            exception.callID = id;

            throw exception;
        }
    }

    override void handleError(RPCWebSocket rpcWebSocket, Exception exception) nothrow @trusted {
        auto response = getErrorResponse(exception);

        try {
            rpcWebSocket.webSocket.send(response);
        } catch (Throwable e) {
            logError("%s", e);
        }
    }

    private string getErrorResponse(Exception exception) nothrow @trusted {
        try {
            return format!`{"jsonrpc":"2.0","error":%s}`(formatError(exception));
        } catch (Exception e) {
            return `{"jsonrpc":"2.0","error":{"code":-32603,"message":"Internal Server Error"}}`;
        }
    }

    private string formatError(Exception exception) @trusted {
        struct Error {
            long code;
            string message;
        }

        auto parser = cast(ParserException)exception;
        if (parser) {
            return Error(ParserException.code, parser.msg).serializeToJsonString();
        }

        auto invalidRequest = cast(InvalidRequestException)exception;
        if (invalidRequest) {
            string errorStr;

            auto invalidParams = cast(InvalidParamsException)exception;
            if (invalidParams) {
                errorStr = Error(InvalidParamsException.code, invalidParams.msg).serializeToJsonString();
            }

            auto methodNotFound = cast(MethodNotFoundException)exception;
            if (methodNotFound) {
                errorStr = Error(MethodNotFoundException.code, methodNotFound.msg).serializeToJsonString();
            }

            auto custom = cast(CustomJsonRPCException)exception;
            if (custom) {
                errorStr = Error(custom.code, custom.msg).serializeToJsonString();
            }

            if (errorStr == null) {
                errorStr = Error(InvalidRequestException.code, invalidRequest.msg).serializeToJsonString();
            }

            if (invalidRequest.callID.type != Json.Type.undefined) {
                errorStr ~= format!`,"id":%s`(invalidRequest.callID.toString());
            }

            return errorStr;
        }

        return Error(-32603, exception.msg).serializeToJsonString();
    }

    auto call(string name, R, Args...)(RPCWebSocket rpcWebSocket, scope OutgoingWebSocketMessage message, Args args) {
        enum isVoid = is(R == void);

        auto json = Json([
            "jsonrpc": Json("2.0"),
            "method": Json(name),
        ]);

        static if (!isVoid) {
            auto id = nextID();
            json["id"] = id;
        }

        Json[] params;
        foreach (arg; args) {
            params ~= serializeToJson(arg);
        }
        json["params"] = params;

        auto jsonStr = json.toString();
        message.write(jsonStr);

        static if (!isVoid) {
            return (() @trusted {
                return async(() {
                    R result;

                    rpcWebSocket.waitTaskReceive(id, 10.seconds,
                        (Json resultJson) {
                            result = deserializeJson!R(resultJson);
                        },
                        (shared Exception e) {
                            throw e;
                        }
                    );

                    return result;
                });
            })();
        }
    }

    private void handleResponse(RPCWebSocket rpcWebSocket, Json json) {
        auto obj = json.opt!(Json[string]);
        if (obj is null) {
            logError("Response is not a json object");
            return;
        }

        auto idPtr = "id" in obj;
        if (idPtr is null) {
            logError("Missing 'id' in response");
            return;
        }
        auto id = (*idPtr).to!size_t;

        auto taskPtr = rpcWebSocket.getWaitingTask(id);

        // Ignore invalid responses
        // TODO: Maybe add a way to hook this?
        if (taskPtr is null) {
            logError("Received unknown response ID");
            return;
        }
        auto task = *taskPtr;

        auto errorPtr = "error" in obj;
        if (errorPtr !is null) {
            auto error = (*errorPtr).opt!(Json[string]);
            if (error is null) {
                logError("Received 'error' that was not an object");
                return;
            }

            auto codePtr = "code" in error;
            if (codePtr is null) {
                logError("Missing error code");
                return;
            }

            auto code = (*codePtr).opt!long;

            auto msgPtr = "message" in error;
            if (msgPtr is null) {
                logError("Missing error message");
                return;
            }

            auto msg = (*msgPtr).opt!string;

            Exception exception;

            static foreach (T; AliasSeq!(ParserException, InvalidRequestException, MethodNotFoundException, InvalidParamsException)) {
                if (code == T.code) {
                    exception = new T(msg);
                }
            }

            if (exception is null) {
                exception = new CustomJsonRPCException(code, msg);
            }

            (() @trusted { task.send(cast(shared)exception); })();
            return;
        }

        auto resultPtr = "result" in obj;
        if (resultPtr is null) {
            logError("Missing 'result' in response");
            return;
        }
        auto resultJson = *resultPtr;

        // Notify task of response
        (() @trusted { task.send(resultJson); })();
    }

    static if (!isVoid) {
        private void handleCall(scope WebSocket webSocket, Json json, string method) {
            auto idPtr = "id" in json;
            Json id = Json.undefined;
            if (idPtr !is null) {
                id = *idPtr;
            }

            Json result;
            bool hasResult = false;

            static foreach (func; __traits(allMembers, Interface)) {
                static if (is(typeof(mixin("instance." ~ func))) && isCallable!(mixin("instance." ~ func))) {
                    if (method == func) {
                        assert(!hasResult);
                        hasResult = true;

                        try {
                            result = decodeCall!func(json);
                        } catch (InvalidRequestException e) {
                            webSocket.send(getErrorResponse(e));
                            return;
                        } catch (Exception e) {
                            auto exception = (() @trusted => new InvalidRequestException(e.toString(), __FILE__, __LINE__, e))();
                            exception.callID = id;
                            webSocket.send(getErrorResponse(exception));
                            return;
                        }
                    }
                }
            }

            if (!hasResult) throw new MethodNotFoundException(format!`Method '%s' does not exist`(method));

            // Respond if we were given an ID
            if (id.type != Json.Type.undefined) {
                // Construct response
                auto response = Json([
                    "jsonrpc": Json("2.0"),
                    "id": id,
                    "result": result,
                ]);

                webSocket.send((scope message) {
                    auto range = streamOutputRange(message);

                    writeJsonString(range, response);
                });
            }
        }

        private Json decodeCall(string func)(Json json) {
            enum fn = "instance." ~ func;
            alias FN = typeof(mixin(fn));
            enum isVoid = is(ReturnType!FN == void);

            alias defaults = ParameterDefaults!FN;
            Parameters!FN parameters;

            auto paramsPtr = "params" in json;
            static if (parameters.length == 0) {
                if (paramsPtr is null) {
                    static if (isVoid) {
                        mixin(fn)();

                        return Json.emptyObject;
                    } else {
                        return mixin(fn)().serializeToJson();
                    }
                }
            } else {
                if (paramsPtr is null) throw new InvalidParamsException(`Missing 'params'`);
            }

            auto params = *paramsPtr;

            if (params.type is Json.Type.object) {
                auto object = params.get!(Json[string]);

                foreach (index, arg; ParameterIdentifierTuple!FN) {
                    alias paramType = typeof(parameters[index]);

                    auto ptr = arg in object;

                    paramType value;

                    static if (is(defaults[index] == void)) {
                        if (ptr is null) throw new InvalidParamsException(format!`Missing argument '%s'`(arg));

                        value = (*ptr).deserializeJson!paramType;
                    } else {
                        if (ptr is null) {
                            value = defaults[index];
                        } else {
                            value = (*ptr).deserializeJson!paramType;
                        }
                    }

                    parameters[index] = deserializeJson!paramType(*ptr);
                }
            } else {
                auto array = params.get!(Json[]);

                foreach (index, arg; ParameterIdentifierTuple!FN) {
                    alias paramType = typeof(parameters[index]);

                    paramType value;

                    static if (is(defaults[index] == void)) {
                        if (index >= array.length) throw new InvalidParamsException(format!`Missing argument at index %s`(index));

                        value = array[index].deserializeJson!paramType;
                    } else {
                        if (index >= array.length) {
                            value = defaults[index];
                        } else {
                            value = array[index].deserializeJson!paramType;
                        }
                    }

                    parameters[index] = value;
                }
            }

            static if (isVoid) {
                mixin(fn)(parameters);

                return Json.undefined;
            } else {
                return mixin(fn)(parameters).serializeToJson();
            }
        }
    }
}

unittest {
    @safe interface Foo {
        void foo();
    }

    class Instance : Foo {
        size_t fooCalled = 0;

        override void foo() {
            fooCalled += 1;
        }
    }

    auto responder = new Instance;
    auto jsonRPC = new JsonRPC!Foo(responder);
    auto message = Json(["params": Json.emptyArray]);
    jsonRPC.handleCall(null, message, "foo");

    assert(responder.fooCalled == 1);
}

unittest {
    struct S {
        int a;
        float b;
        string c;
    }

    @safe interface I {
        void foo();
        int bar(string);
        S test(int, float, string);
    }

    class Instance : I {
        size_t fooCalled = 0;

        override void foo() {
            fooCalled += 1;
        }
        override int bar(string type) {
            if (type == "foo") return 1;
            if (type == "bar") return 2;
            return 0;
        }
        override S test(int a, float b, string c) {
            return S(a + 1, b / 2.0, c);
        }
    }

    auto responder = new Instance;
    auto jsonRPC = new JsonRPC!I(responder);

    auto router = new URLRouter;
    void handleConnection(scope WebSocket ws) {
        auto rpc = new RPCWebSocket(null);
        rpc.webSocket = ws;

        while (ws.waitForData()) {
            ws.receive((scope message) @safe {
                jsonRPC.handleMessage(rpc, message);
            });
        }
    }
    router.get("/", handleWebSockets(&handleConnection));
    auto settings = new HTTPServerSettings;
    settings.port = 5128;
    settings.bindAddresses = ["127.0.0.1"];
    listenHTTP(settings, router);

    auto cws = connectWebSocket(URL("ws://127.0.0.1:5128"));
    auto rpc = new RPCWebSocket(null);
    rpc.webSocket = cws;

    runTask(() {
        while (cws.waitForData()) {
            cws.receive((scope message) @safe {
                jsonRPC.handleMessage(rpc, message);
            });
        }
    });

    bool completed = false;

    runTask(() @trusted {
        scope(exit) exitEventLoop();

        cws.send((scope message) @safe {
            jsonRPC.call!("foo", void)(rpc, message);
        });

        {
            Future!int future;
            cws.send((scope message) @trusted {
                future = jsonRPC.call!("bar", int)(rpc, message, "foo");
            });
            assert(future.getResult() == 1);

            cws.send((scope message) @trusted {
                future = jsonRPC.call!("bar", int)(rpc, message, "bar");
            });
            assert(future.getResult() == 2);

            cws.send((scope message) @trusted {
                future = jsonRPC.call!("bar", int)(rpc, message, "foobar");
            });
            assert(future.getResult() == 0);
        }

        {
            Future!S future;
            cws.send((scope message) @trusted {
                future = jsonRPC.call!("test", S)(rpc, message, 1, 2.0, "foo");
            });
            assert(future.getResult() == S(2, 1.0, "foo"));
        }

        assert(responder.fooCalled == 1);

        completed = true;
    });

    runEventLoop();

    assert(completed);
}
