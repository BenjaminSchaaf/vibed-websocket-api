module vibe.web.websocket_api;

import std.math;
import std.meta;
import std.traits;
import std.algorithm;

import vibe.d;

public {
    import vibe.web.websocket_api.jsonrpc;

    import vibe.web.websocket_api.internal.future : async, Future;
}

@safe:

private {
    struct RPCWebSocketContext {
        RPCWebSocket rpcWebSocket;
    }

    TaskLocal!RPCWebSocketContext context;
}

class RPCTimeoutException : Exception {
    this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable nextInChain = null) pure nothrow @nogc @safe {
        super(msg, file, line, nextInChain);
    }
}

@property RPCWebSocket rpcWebSocket() { return context.rpcWebSocket; };

final class RPCWebSocket {
    private {
        Task _task;
        Task[size_t] _waitingTasks;

        Protocol _protocol;
        WebSocket _webSocket;
    }

    this(Protocol protocol) {
        _protocol = protocol;
    }

    @property Protocol protocol() { return _protocol; }
    @property WebSocket webSocket() { return _webSocket; }
    @property void webSocket(WebSocket ws) { _webSocket = ws; }

    void connect(URL endpoint) {
        connect(connectWebSocket(endpoint));
    }

    void connect(WebSocket webSocket) {
        _webSocket = webSocket;

        _task = runTask(() {
            run();
        });
    }

    void run(WebSocket webSocket) {
        _webSocket = webSocket;

        run();
    }

    void waitTaskReceive(FNS...)(size_t id, Duration timeout, FNS fns) @trusted {
        _waitingTasks[id] = Task.getThis();

        auto received = receiveTimeout(timeout, fns);
        enforce!RPCTimeoutException(received);
    }

    Task* getWaitingTask(size_t id) {
        return id in _waitingTasks;
    }

    private void run() {
        while (_webSocket.waitForData()) {
            _webSocket.receive((scope message) {
                runTask(() nothrow {
                    context = RPCWebSocketContext(this);

                    try {
                        _protocol.handleMessage(this, message);
                    } catch (WebSocketException e) {
                        // Connection closed or something
                        return;
                    } catch (Exception e) {
                        _protocol.handleError(this, e);
                    }
                });
            });
        }
    }

    void close(short code = 1000, string reason = "") {
        _webSocket.close(code, reason);
    }
}

interface Protocol {
    void handleMessage(RPCWebSocket, scope IncomingWebSocketMessage) @safe;
    void handleError(RPCWebSocket, Exception) @safe nothrow;
}

auto registerWebSocketInterface(alias Protocol = JsonRPC, T)(URLRouter router, string path, T instance) {
    alias interfacesTuple = InterfacesTuple!T;
    alias Interface = interfacesTuple[0];
    static if (interfacesTuple.length > 1) {
        pragma(msg, T, " implements more than one interface, we're choosing the first one: ", Interface);
    }

    void handleConnection(scope WebSocket socket) {
        auto protocol = new Protocol!Interface(instance);

        auto rpc = new RPCWebSocket(protocol);

        rpc.run(socket);
    }

    router.get(path, handleWebSockets(&handleConnection));
}

unittest {
    interface API {
        int foo() @safe;
    }

    class Server : API {
        override int foo() { return 12; }
    }

    auto router = new URLRouter;
    router.registerWebSocketInterface("/", new Server);
    auto settings = new HTTPServerSettings;
    settings.port = 5129;
    settings.bindAddresses = ["127.0.0.1"];
    listenHTTP(settings, router);

    auto ws = connectWebSocket(URL("ws://127.0.0.1:5129"));

    bool compelted = false;
    runTask(() @safe {
        scope(exit) exitEventLoop();

        ws.send(`{"jsonrpc":"2.0","id":2,"method":"foo"}`);
        auto response = parseJsonString(ws.receiveText());
        auto id = response["id"].to!int;
        auto result = response["result"].to!int;

        assert(id == 2);
        assert(result == 12);

        compelted = true;
    });

    runEventLoop();

    assert(compelted);
}

class WebSocketInterfaceClient(ServerInterface, alias Protocol = JsonRPC, ClientInterface = void) : ServerInterface {
    import vibe.internal.meta.codegen : CloneFunction;

    private {
        size_t idCounter = 0;

        bool useTaskSocket = false;
        Protocol!ClientInterface _protocol;
        RPCWebSocket _rpcWebSocket;
    }

    this(string url) {
        this(URL(url));
    }

    this(URL url) {
        this(connectWebSocket(url));
    }

    this(WebSocket webSocket) {
        this._protocol = new Protocol!ClientInterface();
        this._rpcWebSocket = new RPCWebSocket(this._protocol);

        this._rpcWebSocket.connect(webSocket);
    }

    this(RPCWebSocket rpcWebSocket) {
        this._protocol = new Protocol!ClientInterface();

        this._rpcWebSocket = rpcWebSocket;
    }

    this(T)(T arg, ClientInterface client) {
        this(arg);

        this._protocol.instance = client;
    }

    @property Protocol!ClientInterface protocol() { return _protocol; }
    @property RPCWebSocket rpcWebSocket() { return _rpcWebSocket; }
    @property WebSocket webSocket() { return _rpcWebSocket.webSocket; }

    static foreach (func; __traits(allMembers, ServerInterface)) {
        static if (is(typeof(mixin("ServerInterface." ~ func))) && isCallable!(mixin("ServerInterface." ~ func))) {
            mixin CloneFunction!(mixin(func), q{
                alias R = ReturnType!(mixin(func));
                enum params = ParameterIdentifierTuple!(mixin(func));

                template to_mixin(size_t index) {
                    static if (index == params.length) {
                        alias to_mixin = AliasSeq!();
                    } else {
                        alias to_mixin = AliasSeq!(mixin(params[index]), to_mixin!(index + 1));
                    }
                }
                alias paramTuple = to_mixin!0;

                assert(webSocket);

                static if (is(R == void)) {
                    webSocket.send((scope message) @trusted {
                        _protocol.call!(func, R)(_rpcWebSocket, message, paramTuple);
                    });
                } else {
                    Future!R future;
                    webSocket.send((scope message) @trusted {
                        future = _protocol.call!(func, R)(_rpcWebSocket, message, paramTuple);
                    });
                    return (() @trusted { return future.getResult(); })();
                }
            });
        }
    }
}

unittest {
    interface API {
        int foo(string) @safe;
    }

    class Server : API {
        override int foo(string value) {
            return value == "foo" ? 12 : -1;
        }
    }

    auto router = new URLRouter;
    router.registerWebSocketInterface("/", new Server);
    auto settings = new HTTPServerSettings;
    settings.port = 5130;
    settings.bindAddresses = ["127.0.0.1"];
    listenHTTP(settings, router);

    bool compelted = false;
    runTask(() @safe {
        scope(exit) exitEventLoop();

        auto client = new WebSocketInterfaceClient!API("ws://127.0.0.1:5130");
        assert(client.foo("bar") == -1);
        assert(client.foo("foo") == 12);

        compelted = true;
    });

    runEventLoop();

    assert(compelted);
}

unittest {
    interface WebSocketServerAPI {
        int getUserId(string name) @safe;
    }

    interface WebSocketClientAPI {
        string challengeResponse(string challenge) @safe;
    }

    class WebSocketServer : WebSocketServerAPI {
        int getUserId(string name) {
            auto client = new WebSocketInterfaceClient!WebSocketClientAPI(rpcWebSocket);

            if (name == "foo" && client.challengeResponse("foo") == "bar") {
                return 3;
            } else {
                return -1;
            }
        }
    }

    class WebSocketClient : WebSocketClientAPI {
        bool challenged = false;

        string challengeResponse(string challenge) {
            assert(challenge == "foo");
            challenged = true;

            return "bar";
        }
    }

    auto router = new URLRouter;
    router.registerWebSocketInterface!JsonRPC("/", new WebSocketServer);
    auto settings = new HTTPServerSettings;
    settings.port = 5131;
    settings.bindAddresses = ["127.0.0.1"];
    listenHTTP(settings, router);

    bool compelted = false;
    runTask(() {
        scope(exit) exitEventLoop();

        auto client = new WebSocketClient;
        auto api = new WebSocketInterfaceClient!(WebSocketServerAPI, JsonRPC, WebSocketClientAPI)("ws://127.0.0.1:5131", client);

        assert(api.getUserId("foo") == 3);
        assert(api.getUserId("bar") == -1);

        assert(client.challenged);

        compelted = true;
    });

    runEventLoop();

    assert(compelted);
}

unittest {
    @safe interface I {
        float sqrt(float);
    }

    class S : I {
        override float sqrt(float a) {
            enforceJsonRPC(a > 0, 1, "foo");
            return std.math.sqrt(a);
        }
    }

    auto router = new URLRouter;
    router.registerWebSocketInterface!JsonRPC("/", new S);
    auto settings = new HTTPServerSettings;
    settings.port = 5132;
    settings.bindAddresses = ["127.0.0.1"];
    listenHTTP(settings, router);

    bool compelted = false;
    runTask(() {
        scope(exit) exitEventLoop();

        auto api = new WebSocketInterfaceClient!(I, JsonRPC)("ws://127.0.0.1:5132");

        assert(api.sqrt(4) == 2);

        try {
            api.sqrt(-1);

            assert(0);
        } catch (CustomJsonRPCException e) {
            assert(e.code == 1);
            assert(e.msg == "foo");
        }

        compelted = true;
    });

    runEventLoop();

    assert(compelted);
}

unittest {
    @safe interface I {
        int mul(int, int);
    }

    class S : I {
        override int mul(int a, int b) {
            return a * b;
        }
    }

    auto router = new URLRouter;
    router.registerWebSocketInterface!JsonRPC("/", new S);
    auto settings = new HTTPServerSettings;
    settings.port = 5133;
    settings.bindAddresses = ["127.0.0.1"];
    listenHTTP(settings, router);

    bool completed = false;
    runTask(() {
        scope(exit) exitEventLoop;

        auto ws = connectWebSocket(URL("ws://127.0.0.1:5133"));

        {
            ws.send(`{"jsonrpc":"2.0","id":1,"method":"mul","params":[2,"foo"]}`);
            const response = ws.receiveText();

            assert(response.canFind(`"code":-32603`));
        }

        {
            ws.send(`{"jsonrpc":"1.1","id":1,"method":"mul","params":[2,1]}`);
            const response = ws.receiveText();

            assert(response.canFind(`"code":-32600`));
        }

        // TODO: More tests here

        completed = true;
    });

    runEventLoop();
    assert(completed);
}
